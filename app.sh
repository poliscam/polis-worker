#!/usr/bin/env bash

start() {
    nohup java -XX:+UseG1GC -jar worker.jar > logs/console.log &
}

stop() {
    PID=$(cat application.pid)
    kill $PID
}

restart() {
    stop
    start
}

case $1 in
start)
  start
   ;;
stop)
  stop
   ;;
restart)
  restart
   ;;
esac