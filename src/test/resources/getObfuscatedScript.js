var execute = function() {
    var data = unpacker_filter(service.getHttp('https://live.entix.io/leader/trikita2.js?width=1093'));
    var token = data.match(/token:'(.*?)'/i);
    log.info('Token is: ' + token[1]);
    result.setUrl(camera.getRealtimeUrl().replace("TOKEN_HERE", token[1]));
    return result;
};

var atob = function (a) {
    var b, c, d, e = {}, f = 0, g = 0, h = "", i = String.fromCharCode, j = a.length;
    for (b = 0; 64 > b; b++) e["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(b)] = b;
    for (c = 0; j > c; c++) for (b = e[a.charAt(c)], f = (f << 6) + b, g += 6; g >= 8; ) ((d = 255 & f >>> (g -= 8)) || j - 2 > c) && (h += i(d));
    return h;
};

function unpacker_filter(source) {
    var stripped_source = trim_leading_comments(source);
    var unpacked = '';

    if (P_A_C_K_E_R.detect(stripped_source)) {
        unpacked = P_A_C_K_E_R.unpack(stripped_source);
        if (unpacked !== stripped_source) {
            return unpacker_filter(unpacked);
        }
    }

    return source;
}

function trim_leading_comments(str)
{
    // very basic. doesn't support /* ... */
    str = str.replace(/^(\s*\/\/[^\n]*\n)+/, '');
    str = str.replace(/^\s+/, '');
    return str;
}

var P_A_C_K_E_R = {
    detect: function (str) {
        return (P_A_C_K_E_R.get_chunks(str).length > 0);
    },

    get_chunks: function(str) {
        var chunks = str.match(/eval\(\(?function\(.*?(,0,\{\}\)\)|split\('\|'\)\)\))($|\n)/g);
        return chunks ? chunks : [];
    },

    unpack: function (str) {
        var chunks = P_A_C_K_E_R.get_chunks(str),
            chunk;
        for(var i = 0; i < chunks.length; i++) {
            chunk = chunks[i].replace(/\n$/, '');
            str = str.split(chunk).join( P_A_C_K_E_R.unpack_chunk(chunk) );
        }
        return str;
    },

    unpack_chunk: function (str) {
        var unpacked_source = '';
        var __eval = eval;
        if (P_A_C_K_E_R.detect(str)) {
            try {
                eval = function (s) { unpacked_source += s; return unpacked_source; };
                __eval(str);
                if (typeof unpacked_source == 'string' && unpacked_source) {
                    str = unpacked_source;
                }
            } catch (e) {
                log.error("ERROR: " + e);
                // well, it failed. we'll just return the original, instead of crashing on user.
            }
        }
        eval = __eval;
        return str;
    }
};
