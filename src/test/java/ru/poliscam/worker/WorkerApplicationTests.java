package ru.poliscam.worker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.ModificationResult;
import ru.poliscam.worker.services.ScriptService;

import java.io.IOException;
import java.nio.file.Files;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkerApplicationTests {

    @Autowired
    private ScriptService scriptService;

    @Value("classpath:getObfuscatedScript.js")
    private Resource obfuscatedScript;

    @Test
    public void script1() throws IOException {
        CameraForWorkerDTO camera = new CameraForWorkerDTO();
        camera.setFfmpegScript(new String(Files.readAllBytes(obfuscatedScript.getFile().toPath())));
        camera.setRealtimeUrl("https://aria.entix.io/play/TOKEN_HERE/trikita2_2/index.m3u8");

        ModificationResult result = scriptService.runScript(camera);

        assert result != null;
    }

}
