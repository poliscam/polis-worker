package ru.poliscam.worker.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@EqualsAndHashCode
public class TaskDTO implements Serializable {
    private static final long serialVersionUID = -1315924858552116374L;
    private String period;
    private String clazz;
}
