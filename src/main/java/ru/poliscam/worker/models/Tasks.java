package ru.poliscam.worker.models;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
public class Tasks implements Serializable {
    private static final long serialVersionUID = 6299184755528116645L;
    private List<CameraForWorkerDTO> cameras = new ArrayList<>();
}
