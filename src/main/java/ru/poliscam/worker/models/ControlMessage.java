package ru.poliscam.worker.models;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ControlMessage implements Serializable {
    private static final long serialVersionUID = 6899084754528216645L;
    private Long identifier;
    private List<Command> commands = new ArrayList<>();

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class Command implements Serializable {
        private static final long serialVersionUID = 4536561577962723272L;
        private CommandType type;
        private CameraForWorkerDTO camera;
        private String clazz;
        private Map<String, String> data = new HashMap<>();

        public enum CommandType {
            RELOAD_WORKER,
            RUN_TASK,
            ADD_TASK,
            REMOVE_TASK,
            RESTART,
            UPDATE,
        }
    }
}
