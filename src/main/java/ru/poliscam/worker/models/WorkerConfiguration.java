package ru.poliscam.worker.models;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class WorkerConfiguration implements Serializable {
    private static final long serialVersionUID = 8462050675060413889L;

    private List<YoutubeCredential> youtubeCredentials = new ArrayList<>();
    private GraphiteConfiguration graphite;
    private String ffmpegPath;
    private String ffprobePath;
    private String videoDirPath;

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class GraphiteConfiguration implements Serializable {
        private static final long serialVersionUID = -5209003934577482957L;
        private String serverAddress;
        private Integer serverPort;
        private Long step;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class YoutubeCredential implements Serializable {
        private static final long serialVersionUID = -7209003935577482957L;
        private String name;
        private String clientId;
        private String clientSecret;
        private String refreshToken;
    }
}

