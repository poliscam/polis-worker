package ru.poliscam.worker.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class CameraForWorkerDTO implements Serializable {
    private static final long serialVersionUID = 3788658188433195174L;
    private Long id;
    private String complexName;
    private String name;
    private String internalName;
    private Boolean useCurl = false;
    private String realtimeUrl;
    private Integer width = 800;
    private Integer height = 600;

    private String ffmpegScript;

    private List<TaskDTO> tasks = new ArrayList<>();
}
