package ru.poliscam.worker.models;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class IdentifierMessage implements Serializable {
    private static final long serialVersionUID = 6399084755528116645L;
    private Long identifier;
}
