package ru.poliscam.worker.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@EqualsAndHashCode
public class IpAddressDTO implements Serializable {
    private static final long serialVersionUID = -1316924858552216374L;
    private String ip;
}
