package ru.poliscam.worker;

import lombok.extern.log4j.Log4j2;
import net.bramp.ffmpeg.progress.ProgressListener;
import org.apache.logging.log4j.core.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

@Log4j2
public class Utils {
    private static PositiveRandom random = new PositiveRandom(1000000);

    public static File createInputConcatFile(Collection<String> files) throws IOException {
        FileUtils.mkdir(new File("temp/"), true);
        File path = new File("temp/ffmpeg-input-data-" + random.nextNonNegative() + ".tmp");
        List<String> lines = new ArrayList<>();

        for (String file : files) {
            lines.add("file '" + file + "'");
        }

        log.debug("[x] Creating temp input file: " + path);

        Path temp = path.toPath();
        try {
            Files.write(temp, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            log.error("[-] Error happens while write temp input file!", e);
        }

        return path;
    }

    public static ProgressListener createProgressListener(String description, double duration_ns) {
        return progress -> {
            double percentage = progress.out_time_ns / duration_ns;
            log.info(String.format(description + ": [%.0f%%], status: %s",
                    percentage * 100,
                    progress.status
            ));
        };
    }

    public static void deleteTempFile(String path) {
        log.debug("[x] Deleting temp input file: {}", path);
        new File(path).delete();
    }

    private static class PositiveRandom extends Random {
        PositiveRandom(int seed) {
            super(seed);
        }

        int nextNonNegative() {
            return next(Integer.SIZE - 1);
        }
    }

}
