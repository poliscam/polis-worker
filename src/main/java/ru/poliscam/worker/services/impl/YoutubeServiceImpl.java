package ru.poliscam.worker.services.impl;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatus;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.configs.RabbitMqConfig;
import ru.poliscam.worker.exceptions.FailedToUploadException;
import ru.poliscam.worker.exceptions.LimitExceededException;
import ru.poliscam.worker.exceptions.NoMoreCredentialsException;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.VideoUploaderMessage;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.CredentialService;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.YoutubeService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Nikolay Viguro 22.10.18
 */

@Service
@Log4j2
public class YoutubeServiceImpl implements YoutubeService {
    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final Random random = new Random();

    private static final int HTTP_BAD_REQUEST_CODE = 400;
    private static final int HTTP_FORBIDDEN_CODE = 403;
    private static final String LIMIT_EXCEEDED_ERROR = "uploadLimitExceeded";
    private static final String QUOTA_EXCEEDED_ERROR = "quotaExceeded";

    private Cache<String, Integer> usedTime = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.HOURS)
            .build();

    // todo save state between restarts
    private Cache<String, Credential> exceededCredentials = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.HOURS)
            .build();

    @Value("${worker.id}")
    private Long worker;

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    private static MediaHttpUploaderProgressListener createProgressListener(String cam) {
        return uploader -> {
            switch (uploader.getUploadState()) {
                case INITIATION_STARTED:
                    log.info("[{}] Initiation started", cam);
                    break;
                case INITIATION_COMPLETE:
                    log.info("[{}] Initiation completed", cam);
                    break;
                case MEDIA_IN_PROGRESS:
                    log.info("[{}] Uploaded: {}%", cam, String.format("%.2f", uploader.getProgress() * 100));
                    break;
                case MEDIA_COMPLETE:
                    log.info("[{}] Upload completed", cam);
                    break;
                case NOT_STARTED:
                    log.info("[{}] Upload not started", cam);
                    break;
            }
        };
    }

    @PostConstruct
    void init() {
        // todo restore times of exceeded creds
        this.exceededCredentials = metricsService.gaugeCollectionSize(Consts.GAUGE_CREDENTIALS_EXCEEDED_COUNT, exceededCredentials);
    }

    @PreDestroy
    void destory() {
        // todo save time of exceeded creds
    }

    @Override
    public String uploadDaily(CameraForWorkerDTO camera) throws FailedToUploadException {
        Calendar cal = Calendar.getInstance();

        String id = upload(
                camera,
                camera.getComplexName() + " - " + camera.getName() + " (" + format.format(cal.getTime()) + ") - daily",
                "Дневное видео с камеры сохранено в " + cal.getTime(),
                new File(configService.getConfiguration(false).getVideoDirPath() + "/" + camera.getInternalName() + "/daily.mp4"),
                "daily", camera.getName()
        );

        amqpTemplate.convertAndSend(
                RabbitMqConfig.EXCHANGE,
                RabbitMqConfig.VIDEO_DAILY_KEY,
                VideoUploaderMessage.builder()
                        .worker(worker)
                        .videos(Collections.singletonList(
                                VideoUploaderMessage.UploadedVideo.builder()
                                        .cameraId(camera.getId())
                                        .date(new Date())
                                        .type(VideoUploaderMessage.UploadedVideo.UploadType.DAILY)
                                        .youtubeId(id)
                                        .build()
                        ))
                        .build()
        );

        return id;
    }

    @Override
    public String uploadWeek(CameraForWorkerDTO camera) throws FailedToUploadException {
        Calendar cal = Calendar.getInstance();

        String id = upload(
                camera,
                camera.getComplexName() + " - " + camera.getName() + " (" + format.format(cal.getTime()) + ") - week",
                "Недельное видео с камеры сохранено в " + cal.getTime(),
                new File("temp/" + camera.getInternalName() + "-week-completed.mp4"),
                "week", camera.getName()
        );

        amqpTemplate.convertAndSend(
                RabbitMqConfig.EXCHANGE,
                RabbitMqConfig.VIDEO_WEEKLY_KEY,
                VideoUploaderMessage.builder()
                        .worker(worker)
                        .videos(Collections.singletonList(
                                VideoUploaderMessage.UploadedVideo.builder()
                                        .cameraId(camera.getId())
                                        .date(new Date())
                                        .type(VideoUploaderMessage.UploadedVideo.UploadType.WEEKLY)
                                        .youtubeId(id)
                                        .build()
                        ))
                        .build()
        );

        return id;
    }

    @Override
    public String uploadAllTime(CameraForWorkerDTO camera) throws FailedToUploadException {
        Calendar cal = Calendar.getInstance();

        String id = upload(
                camera,
                camera.getComplexName() + " - " + camera.getName() + " (" + format.format(cal.getTime()) + ") - alltime",
                "Обзорное видео с камеры сохранено в " + cal.getTime(),
                new File("temp/" + camera.getInternalName() + "-fast-completed.mp4"),
                "all", camera.getName()
        );

        amqpTemplate.convertAndSend(
                RabbitMqConfig.EXCHANGE,
                RabbitMqConfig.VIDEO_ALLTIME_KEY,
                VideoUploaderMessage.builder()
                        .worker(worker)
                        .videos(Collections.singletonList(
                                VideoUploaderMessage.UploadedVideo.builder()
                                        .cameraId(camera.getId())
                                        .date(new Date())
                                        .type(VideoUploaderMessage.UploadedVideo.UploadType.ALL_TIME)
                                        .youtubeId(id)
                                        .build()
                        ))
                        .build()
        );

        return id;
    }

    private String upload(CameraForWorkerDTO camera, String title, String desc, File initialFile, String... tags)
            throws FailedToUploadException {
        CredentialWithId credential = null;
        try {
            credential = getNextCredential();
            return uploadInternal(camera, credential.getCredential(), title, desc, initialFile, tags);
        } catch (LimitExceededException ex) {
            markCredentialAsExceeded(credential);
            return upload(camera, title, desc, initialFile, tags);
        } catch (NoMoreCredentialsException ex) {
            log.error("[-] No more credentials available for using at this time!");
            throw new FailedToUploadException("No more credentials available for using");
        }
    }

    private String uploadInternal(CameraForWorkerDTO camera, Credential credential, String title, String desc, File initialFile, String... tags)
            throws LimitExceededException, FailedToUploadException {
        Video videoObjectDefiningMetadata = new Video();
        VideoStatus status = new VideoStatus();
        VideoSnippet snippet = new VideoSnippet();

        YouTube youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName("poliscam-uploader")
                .build();

        status.setPrivacyStatus("unlisted");
        videoObjectDefiningMetadata.setStatus(status);

        snippet.setTitle(title);
        snippet.setDescription(desc);

        List<String> tagsToVideo = new ArrayList<>();
        tagsToVideo.add("poliscam");
        tagsToVideo.addAll(Arrays.asList(tags));

        snippet.setTags(tagsToVideo);

        videoObjectDefiningMetadata.setSnippet(snippet);

        InputStream videoStream;
        try {
            videoStream = new BufferedInputStream(new FileInputStream(initialFile));
        } catch (FileNotFoundException e) {
            throw new FailedToUploadException("Video file not found");
        }
        AbstractInputStreamContent videoContent = new InputStreamContent("video/*", videoStream).setLength(initialFile.length());

        try {
            YouTube.Videos.Insert videoInsert = youtube.videos()
                    .insert("snippet,statistics,status", videoObjectDefiningMetadata, videoContent);

            MediaHttpUploader uploader = videoInsert.getMediaHttpUploader();
            uploader.setDirectUploadEnabled(false);
            uploader.setProgressListener(createProgressListener(camera.getInternalName()));

            log.info("[{}] Start upload video", camera.getInternalName());

            Video returnedVideo = videoInsert.execute();

            if(returnedVideo == null) {
                throw new FailedToUploadException("Returned video is null");
            }

            log.info("[{}] Uploaded successfully: {}", camera.getInternalName(), returnedVideo.getSnippet().getTitle());

            log.info("[{}] Status: {}", camera.getInternalName(), returnedVideo.getStatus().getUploadStatus());
            log.info("[{}] Rejected: {}", camera.getInternalName(), returnedVideo.getStatus().getRejectionReason());
            log.info("[{}] Failure: {}", camera.getInternalName(), returnedVideo.getStatus().getFailureReason());

            // todo handle error codes
            if (!StringUtils.isEmpty(returnedVideo.getStatus().getRejectionReason())) {
                throw new FailedToUploadException();
            }
            if (!StringUtils.isEmpty(returnedVideo.getStatus().getFailureReason())) {
                throw new FailedToUploadException();
            }

            return returnedVideo.getId();
        } catch (IOException e) {
            if(e instanceof GoogleJsonResponseException) {
                GoogleJsonResponseException ex = (GoogleJsonResponseException) e;
                if(ex.getDetails() != null) {
                    switch (ex.getDetails().getCode()) {
                        case HTTP_FORBIDDEN_CODE:
                        case HTTP_BAD_REQUEST_CODE: {
                            boolean exceeded = ex.getDetails().getErrors().stream().anyMatch(
                                    err -> err.getReason().equals(LIMIT_EXCEEDED_ERROR)
                                    || err.getReason().equals(QUOTA_EXCEEDED_ERROR)
                            );
                            if(exceeded) {
                                log.error("[{}] Youtube tell me that the credentials exceeded", camera.getInternalName());
                                throw new LimitExceededException();
                            } else {
                                log.debug("Unknown error: {}", ((GoogleJsonResponseException) e).getContent(), e);
                                throw new FailedToUploadException("Bad 40X request, but unknown error");
                            }
                        }
                        default: {
                            log.debug("Unknown error: {}", ((GoogleJsonResponseException) e).getContent(), e);
                            throw new FailedToUploadException("Unknown error code: " + ex.getDetails().getCode());
                        }
                    }
                } else {
                    throw new FailedToUploadException("No details shipped to error");
                }
            } else {
                throw new FailedToUploadException("Unknown IO error");
            }
        }
    }

    private CredentialWithId getNextCredential() throws NoMoreCredentialsException {
        synchronized (this) {
            Map<String, Credential> credentialMap = credentialService.getCredentials();
            Map<String, Credential> aliveCredentials = new HashMap<>();

            credentialMap.forEach((id, credential) -> {
                if (exceededCredentials.getIfPresent(id) == null) {
                    log.debug("[x] Found alive cred: {}", id);
                    aliveCredentials.put(id, credential);
                } else {
                    log.debug("[x] Found exceeded cred: {} - {}", id, credential);
                }
            });

            if (CollectionUtils.isEmpty(aliveCredentials)) {
                throw new NoMoreCredentialsException();
            } else {
                List<String> keys = new ArrayList<>(aliveCredentials.keySet());
                String randomKey = keys.get(random.nextInt(keys.size()));

                if(usedTime.getIfPresent(randomKey) != null && usedTime.getIfPresent(randomKey).equals(25)) {
                    log.error("[-] Credential {} is used 25 times. Skip to next", randomKey);
                    exceededCredentials.put(randomKey, aliveCredentials.get(randomKey));
                    return getNextCredential();
                }

                if(usedTime.getIfPresent(randomKey) != null) {
                    usedTime.put(randomKey, usedTime.getIfPresent(randomKey) + 1);
                } else {
                    usedTime.put(randomKey, 1);
                }

                log.info("[x] Credential will be used: {}", randomKey);
                return new CredentialWithId(randomKey, aliveCredentials.get(randomKey));
            }
        }
    }

    private void markCredentialAsExceeded(CredentialWithId credential) {
        synchronized (this) {
            if(exceededCredentials.getIfPresent(credential.getId()) == null) {
                log.warn("[-] Credential marked as exceeded: {}", credential.getId());
                exceededCredentials.put(credential.getId(), credential.getCredential());
            } else {
                log.warn("[-] Credential already marked as exceeded", credential.getId());
            }
        }
    }

    @AllArgsConstructor
    @Getter
    private class CredentialWithId {
        private String id;
        private Credential credential;
    }
}
