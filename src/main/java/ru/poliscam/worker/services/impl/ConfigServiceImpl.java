package ru.poliscam.worker.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import ru.poliscam.worker.configs.RabbitMqConfig;
import ru.poliscam.worker.models.IpAddressDTO;
import ru.poliscam.worker.models.RegistrationMessage;
import ru.poliscam.worker.models.Tasks;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.MetricsService;

import java.math.BigDecimal;

@Service
@Log4j2
@PropertySource(ignoreResourceNotFound = true, value = "classpath:version.properties")
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AmqpTemplate template;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MetricsService metricsService;

    @Value("${worker.id}")
    private Long worker;

    @Getter
    @Value("${worker.version}")
    private BigDecimal version;

    @Getter
    private String ip;

    private WorkerConfiguration configuration;
    private Tasks tasks;

    private volatile boolean initialized = false;

    @Override
    public boolean initialized() {
        if(!initialized) {
            if(configuration != null && tasks != null) {
                initialized = true;
            }
        }
        return initialized;
    }

    @Override
    public synchronized WorkerConfiguration getConfiguration(boolean force) {
        if (configuration == null || force) {
            configuration = doRegisterOnServer();
        }
        return configuration;
    }

    @Override
    public synchronized Tasks getTasks(boolean force) {
        if (tasks == null || force) {
            tasks = getTasksFromServer();
        }
        return tasks;
    }

    private WorkerConfiguration doRegisterOnServer() {
        log.info("[x] Fetching configuration from server");

        try {
            IpAddressDTO ipAnswer = restTemplate.getForObject("https://api.ipify.org?format=json", IpAddressDTO.class);
            if(ipAnswer != null && !StringUtils.isEmpty(ipAnswer.getIp())) {
                ip = ipAnswer.getIp();
                log.info("[x] Worker IP: {}", ip);
            } else {
                log.error("[-] Null or empty answer about IP address");
            }
        }
        catch (Exception e) {
            log.error("[-] Error while trying to fetch current ip", e);
        }

        try {
            String config = (String) template.convertSendAndReceive(
                    RabbitMqConfig.EXCHANGE,
                    RabbitMqConfig.REGISTER_KEY,
                    new RegistrationMessage(worker, ip, version)
            );

            if (StringUtils.isEmpty(config)) {
                log.error("[-] Can't register on server - empty answer!");
                Thread.sleep(10_000L);
                return doRegisterOnServer();
            } else {
                return objectMapper.readValue(config, WorkerConfiguration.class);
            }
        } catch (Exception e) {
            log.error("[-] Can't register on server - exception happens!", e);
            try {
                Thread.sleep(10_000L);
            } catch (InterruptedException ignored) {
            }
            return doRegisterOnServer();
        }
    }

    private Tasks getTasksFromServer() {
        log.info("[x] Fetching cameras and tasks from server");

        String message;
        try {
            message = (String) template.convertSendAndReceive(
                    RabbitMqConfig.EXCHANGE,
                    RabbitMqConfig.TASKS_KEY,
                    worker
            );

            if (StringUtils.isEmpty(message)) {
                log.error("[-] Can't get tasks from server - empty answer!");
                Thread.sleep(10_000L);
                return getTasksFromServer();
            } else {
                return objectMapper.readValue(message, Tasks.class);
            }
        } catch (Exception e) {
            log.error("[-] Can't get tasks from server - empty answer!");
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException ignored) {
            }
            return getTasksFromServer();
        }
    }
}
