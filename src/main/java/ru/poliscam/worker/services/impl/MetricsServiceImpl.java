package ru.poliscam.worker.services.impl;

import com.google.common.cache.Cache;
import io.micrometer.core.instrument.*;
import io.micrometer.graphite.GraphiteMeterRegistry;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poliscam.worker.services.MetricsService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Log4j2
public class MetricsServiceImpl implements MetricsService {
    @Autowired
    private GraphiteMeterRegistry registry;

    private Map<String, Counter> counterMap = Collections.synchronizedMap(new HashMap<>());
    private Map<String, Object> gaugeSingletonMap = Collections.synchronizedMap(new HashMap<>());
    private Map<String, AtomicInteger> gaugeList = Collections.synchronizedMap(new HashMap<>());
    private Map<String, AtomicBoolean> gaugeBoolList = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void incrementCounter(String counterName, Tag tag) {
        Counter counter = Counter.builder(counterName).tags(Tags.of(tag)).register(registry);
        counter.increment();
    }

    @Override
    public <T extends Collection> T gaugeCollectionSize(String counterName, T collection) {
        registry.gauge(counterName, Collections.emptyList(), collection, c -> (double) collection.size());
        return collection;
    }

    @Override
    public <T extends Collection> T gaugeCollectionSize(String counterName, T collection, Tag tag) {
        registry.gauge(counterName, Tags.of(tag), collection, c -> (double) collection.size());
        return collection;
    }

    @Override
    public <T extends Map> T gaugeCollectionSize(String counterName, T collection) {
        registry.gauge(counterName, Collections.emptyList(), collection, c -> (double) collection.keySet().size());
        return collection;
    }

    @Override
    public <T extends Map> T gaugeCollectionSize(String counterName, T collection, Tag tag) {
        registry.gauge(counterName, Tags.of(tag), collection, c -> (double) collection.keySet().size());
        return collection;
    }

    @Override
    public <T extends Cache> T gaugeCollectionSize(String counterName, T collection) {
        registry.gauge(counterName, Collections.emptyList(), collection, c -> (double) collection.asMap().keySet().size());
        return collection;
    }

    @Override
    public <T extends Cache> T gaugeCollectionSize(String counterName, T collection, Tag tag) {
        registry.gauge(counterName, Tags.of(tag), collection, c -> (double) collection.asMap().keySet().size());
        return collection;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Number> T gaugeSingleton(String counterName, T number) {
        T key = (T) gaugeSingletonMap.get(counterName);

        if (key == null) {
            key = registry.gauge(counterName, number);
            gaugeSingletonMap.put(counterName, key);
            return key;
        } else {
            return key;
        }
    }

    @Override
    public AtomicInteger gaugeInteger(String counterName, Tag tag) {
        AtomicInteger integer = gaugeList.get(counterName + "-" + tag.getKey() + "-" + tag.getValue());

        if (integer == null) {
            integer = new AtomicInteger(0);
            Gauge.builder(counterName, integer, AtomicInteger::get)
                    .tags(Tags.of(tag))
                    .register(registry);
            gaugeList.put(counterName + "-" + tag.getKey() + "-" + tag.getValue(), integer);
        }

        return integer;
    }

    @Override
    public AtomicBoolean bool(String counterName, boolean val, Tag tag) {
        AtomicBoolean bool = gaugeBoolList.get(counterName + "-" + tag.getKey() + "-" + tag.getValue());

        if (bool == null) {
            bool = new AtomicBoolean(val);
            Gauge.builder(counterName, bool, b -> b.get() ? 1.0D : 0.0D)
                    .tags(Tags.of(tag))
                    .register(registry);
            gaugeBoolList.put(counterName + "-" + tag.getKey() + "-" + tag.getValue(), bool);
        }

        return bool;
    }

    @Override
    public Timer timer(String counterName, Tag tag) {
        return Timer.builder(counterName)
                .tags(Tags.of(tag))
                .register(registry);
    }
}
