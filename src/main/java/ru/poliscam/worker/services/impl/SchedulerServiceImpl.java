package ru.poliscam.worker.services.impl;

import lombok.extern.log4j.Log4j2;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.services.SchedulerService;

import java.util.UUID;

import static org.quartz.TriggerBuilder.newTrigger;

@Service
@Log4j2
public class SchedulerServiceImpl implements SchedulerService {
    @Autowired
    private Scheduler scheduler;

    @Override
    public void createTask(Class<? extends QuartzJobBean> jobClass, String cron, CameraForWorkerDTO camera) throws SchedulerException {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("camera", camera);

        JobKey key = new JobKey(jobClass.getSimpleName() + "-camera-" + camera.getInternalName());
        JobDetail jobDetail = buildJobDetail(jobClass, jobDataMap, key);
        Trigger trigger = buildJobTrigger(jobDetail, cron);
        scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public void runTaskNow(Class<? extends QuartzJobBean> jobClass, CameraForWorkerDTO camera) throws SchedulerException {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("camera", camera);

        JobKey key = new JobKey(UUID.randomUUID() + "-run-once");
        JobDetail jobDetail = buildJobDetail(jobClass, jobDataMap, key);
        Trigger trigger = newTrigger()
                .withIdentity(UUID.randomUUID() + "-trigger-run-once")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                .withRepeatCount(0)
                                .withIntervalInMinutes(10))
                .startNow()
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public void cancelAllTasks() throws SchedulerException {
        scheduler.clear();
    }

    @Override
    public void cancelTask(Class<? extends QuartzJobBean> jobClass, CameraForWorkerDTO camera) throws SchedulerException {
        JobKey key = new JobKey(jobClass.getSimpleName() + "-camera-" + camera.getInternalName());
        scheduler.deleteJob(key);
    }

    private JobDetail buildJobDetail(Class<? extends QuartzJobBean> jobClass, JobDataMap jobDataMap, JobKey key) {
        return JobBuilder.newJob(jobClass)
                .withIdentity(key)
                .withDescription("Worker Job")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, String period) {
        return newTrigger()
                .forJob(jobDetail)
                .withIdentity(UUID.randomUUID().toString(), "worker-triggers")
                .withDescription("Worker Trigger")
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule(period))
                .build();
    }
}
