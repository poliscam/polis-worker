package ru.poliscam.worker.services.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.ModificationResult;
import ru.poliscam.worker.services.ScriptService;

import javax.script.*;

@Service

@Log4j2
public class ScriptServiceImpl implements ScriptService {
    @Autowired
    private RestTemplate rest;

    private static final String ENGINE_NAME = "nashorn";
    private static final String FUNCTION_NAME = "execute";

    @Override
    public ModificationResult runScript(CameraForWorkerDTO camera) {
        try {
            ScriptEngine engine = new ScriptEngineManager().getEngineByName(ENGINE_NAME);
            Bindings bindings = engine.getContext().getBindings(ScriptContext.ENGINE_SCOPE);

            bindings.put("log", log);
            bindings.put("service", this);
            bindings.put("camera", camera);
            bindings.put("result", new ModificationResult());

            engine.eval(camera.getFfmpegScript());
            return (ModificationResult) ((Invocable)engine).invokeFunction(FUNCTION_NAME);
        } catch (ScriptException e) {
            log.error("[{}] Script execution error: ", camera.getInternalName(), e);
        } catch (NoSuchMethodException e) {
            log.error("[{}] Error: ", camera.getInternalName(), e);
        }

        return null;
    }

    // methods for script
    @Override
    public String getHttp(String url) {
        return rest.getForObject(url, String.class);
    }
}
