package ru.poliscam.worker.services.impl;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.DataStoreCredentialRefreshListener;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.FileDataStoreFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.CredentialService;
import ru.poliscam.worker.services.MetricsService;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CredentialServiceImpl implements CredentialService {
    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Value("${worker.id}")
    private Long worker;

    private Map<String, Credential> credentialsMap = Collections.synchronizedMap(new HashMap<>());

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();

    @Async
    public void init() throws InterruptedException {
        log.info("[x] Wait for initialization");
        while (!configService.initialized()) {
            Thread.sleep(1000L);
        }

        this.credentialsMap = metricsService.gaugeCollectionSize(Consts.GAUGE_CREDENTIALS_COUNT, credentialsMap);

        log.info("[x] Loading and refreshing youtube credentials");
        configService.getConfiguration(false).getYoutubeCredentials().forEach(data -> {
            try {
                File currentCredDir = new File(Consts.CONFIG_DIR);
                FileUtils.mkdir(currentCredDir, true);

                FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(currentCredDir);
                DataStore<StoredCredential> dataStore = fileDataStoreFactory.getDataStore("credentials");

                GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
                GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();
                details.setClientId(data.getClientId());
                details.setClientSecret(data.getClientSecret());
                clientSecrets.setInstalled(details);

                GoogleCredential credential = new GoogleCredential.Builder()
                        .setTransport(HTTP_TRANSPORT)
                        .setJsonFactory(JSON_FACTORY)
                        .setClientSecrets(clientSecrets)
                        .addRefreshListener(
                                new DataStoreCredentialRefreshListener(
                                        Consts.USER, dataStore
                                )
                        )
                        .build();

                if(!StringUtils.isEmpty(data.getRefreshToken())){
                    credential.setRefreshToken(data.getRefreshToken());
                } else{
                    log.error("[-] No refresh token supplied with credentials \"{}\"", data.getName());
                }

                if(!credential.refreshToken()) {
                    log.error("[-] Token for \"{}\" NOT refreshed!", data.getName());
                }
                else {
                    log.info("[x] Token for \"{}\" refreshed!", data.getName());
                    credentialsMap.put(data.getName(), credential);
                }

                saveCredential(dataStore, data, credential);
            } catch (IOException e) {
                log.error("[-] Issue while setting credentials", e);
            }
        });
        log.info("[x] Refreshing youtube credentials done");
    }

    @Override
    public void refresh() {
        try {
            init();
        } catch (InterruptedException e) {
            log.error("[-] Unable to refresh data");
        }
    }

    @Override
    public Map<String, Credential> getCredentials() {
        return credentialsMap;
    }

    private void saveCredential(DataStore<StoredCredential> dataStore, WorkerConfiguration.YoutubeCredential data, Credential credential){
        StoredCredential storedCredential = new StoredCredential();
        storedCredential.setAccessToken(credential.getAccessToken());
        storedCredential.setRefreshToken(credential.getRefreshToken());
        try {
            dataStore.set(data.getName(), storedCredential);
        } catch (IOException e) {
            log.error("[-] IO exception: ", e);
        }
    }
}
