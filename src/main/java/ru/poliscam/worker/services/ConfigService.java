package ru.poliscam.worker.services;

import ru.poliscam.worker.models.Tasks;
import ru.poliscam.worker.models.WorkerConfiguration;

import java.math.BigDecimal;

public interface ConfigService {
    WorkerConfiguration getConfiguration(boolean force);
    boolean initialized();
    Tasks getTasks(boolean force);
    String getIp();
    BigDecimal getVersion();
}
