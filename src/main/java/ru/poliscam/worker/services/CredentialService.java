package ru.poliscam.worker.services;

import com.google.api.client.auth.oauth2.Credential;

import java.util.Map;

/**
 * @author Nikolay Viguro 22.10.18
 */
public interface CredentialService {
    void refresh();
    Map<String, Credential> getCredentials();
}
