package ru.poliscam.worker.services;

import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import ru.poliscam.worker.models.CameraForWorkerDTO;

public interface SchedulerService {
    void createTask(Class<? extends QuartzJobBean> jobClass, String cron, CameraForWorkerDTO camera) throws SchedulerException;

    void runTaskNow(Class<? extends QuartzJobBean> jobClass, CameraForWorkerDTO camera) throws SchedulerException;

    void cancelAllTasks() throws SchedulerException;

    void cancelTask(Class<? extends QuartzJobBean> jobClass, CameraForWorkerDTO camera) throws SchedulerException;
}
