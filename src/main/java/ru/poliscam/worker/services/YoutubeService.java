package ru.poliscam.worker.services;

import ru.poliscam.worker.exceptions.FailedToUploadException;
import ru.poliscam.worker.models.CameraForWorkerDTO;

import java.io.IOException;

/**
 * @author Nikolay Viguro 22.10.18
 */

public interface YoutubeService {
    String uploadWeek(CameraForWorkerDTO camera) throws IOException, FailedToUploadException;

    String uploadDaily(CameraForWorkerDTO camera) throws IOException, FailedToUploadException;

    String uploadAllTime(CameraForWorkerDTO camera) throws IOException, FailedToUploadException;
}
