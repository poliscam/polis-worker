package ru.poliscam.worker.services;

import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.ModificationResult;

public interface ScriptService {
    ModificationResult runScript(CameraForWorkerDTO camera);

    // methods for script
    String getHttp(String url);

    //String getDeobfuscatedHttp(String url) throws IOException;
}
