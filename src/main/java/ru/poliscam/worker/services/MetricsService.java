package ru.poliscam.worker.services;

import com.google.common.cache.Cache;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public interface MetricsService {
    void incrementCounter(String counterName, Tag tag);

    <T extends Collection> T gaugeCollectionSize(String counterName, T collection);

    <T extends Collection> T gaugeCollectionSize(String counterName, T collection, Tag tag);

    <T extends Map> T gaugeCollectionSize(String counterName, T collection);

    <T extends Map> T gaugeCollectionSize(String counterName, T collection, Tag tag);

    <T extends Cache> T gaugeCollectionSize(String counterName, T collection);

    <T extends Cache> T gaugeCollectionSize(String counterName, T collection, Tag tag);

    <T extends Number> T gaugeSingleton(String counterName, T number);

    AtomicInteger gaugeInteger(String counterName, Tag tag);

    AtomicBoolean bool(String counterName, boolean val, Tag tag);

    Timer timer(String counterName, Tag tag);
}
