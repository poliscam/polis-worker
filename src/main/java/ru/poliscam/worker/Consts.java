package ru.poliscam.worker;

public class Consts {
    public static final String CAMERAS_COUNT = "camera.count.total";
    public static final String TASKS_COUNT = "job.count.total";

    public static final String GAUGE_FAILED_GET_IMG_JOBS = "job.create.image.failed";
    public static final String TIMER_GET_IMG_JOBS = "job.create.image.time";

    public static final String GAUGE_SUCCESS_CREATE_DAILY_VIDEO_JOBS = "job.create.daily.success";
    public static final String TIMER_CREATE_DAILY_VIDEO_JOBS = "job.create.daily.time";
    public static final String TIMER_UPLOAD_DAILY_VIDEO_JOBS = "job.upload.daily.time";

    public static final String TIMER_CREATE_WEEKLY_VIDEO_JOBS = "job.create.weekly.time";
    public static final String TIMER_UPLOAD_WEEKLY_VIDEO_JOBS = "job.upload.weekly.time";
    public static final String TIMER_CREATE_ALLTIME_VIDEO_JOBS = "job.create.alltime.time";
    public static final String TIMER_UPLOAD_ALLTIME_VIDEO_JOBS = "job.upload.alltime.time";

    public static final String GAUGE_CREDENTIALS_COUNT = "creds.total";
    public static final String GAUGE_CREDENTIALS_EXCEEDED_COUNT = "creds.exceeded.total";

    public static final String TAG = "commonTag";
    public static final String CONFIG_DIR = "config";
    public static final String USER = "user";
    public static final String GAUGE_VERSION = "worker.version";
}
