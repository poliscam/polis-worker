package ru.poliscam.worker.jobs.service;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.services.ConfigService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
@Log4j2
public class CleanUpJob {
    @Autowired
    private ConfigService configService;

    @Value("${job.cleanup.ttl:3}")
    private Long ttl;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${job.cleanup.cron:0 30 23 * * ?}")
    public void cleanUp() throws IOException {
        log.info("[x] Starting cleanup");

        List<String> camNames = new ArrayList<>();

        configService.getTasks(false).getCameras().forEach(c-> {
            deleteImages(c);
            camNames.add(c.getInternalName());
        });

        File images = new File("images/");
        if(!images.exists()) {
            return;
        }

        for (final File dir : Objects.requireNonNull(images.listFiles())) {
            if (dir.isDirectory()) {
                boolean delete = true;
                for (String cam : camNames) {
                    if (cam.equals(dir.getName()))
                        delete = false;
                }

                if (delete) {
                    FileUtils.deleteDirectory(dir);
                    log.info("[x] Deleting unnecessary directory - {}", dir.getName());
                }
            }
        }


        log.info("[x] Done cleanup");
    }

    private void deleteImages(CameraForWorkerDTO cam) {
        File dir = new File("images/" + cam.getInternalName());

        if(!dir.exists()) {
            return;
        }

        File[] directories = dir.listFiles(File::isDirectory);
        for (File file : Objects.requireNonNull(directories)) {
            long diff = new Date().getTime() - file.lastModified();

            if (diff > ttl * 24 * 60 * 60 * 1000) {
                log.info("[x] Deleting: {}", file.getAbsolutePath());
                try {
                    FileUtils.deleteDirectory(file);
                } catch (IOException e) {
                    log.error("[-] Can't delete directory: {}", file.getAbsolutePath(), e);
                }
            }
        }
    }
}
