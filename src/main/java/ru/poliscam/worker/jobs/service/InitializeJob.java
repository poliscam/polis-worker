package ru.poliscam.worker.jobs.service;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.graphite.GraphiteConfig;
import io.micrometer.graphite.GraphiteHierarchicalNameMapper;
import io.micrometer.graphite.GraphiteMeterRegistry;
import lombok.extern.log4j.Log4j2;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.Tasks;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.CredentialService;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.SchedulerService;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Log4j2
public class InitializeJob {
    @Value("${worker.version}")
    private BigDecimal version;

    @Autowired
    private ConfigService configService;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private MetricsService metricsService;

    private WorkerConfiguration configuration;

    private long tasksCount;
    private long camerasCount;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(fixedDelay = 3600_000, initialDelay = 3600_000)
    public void reloadData() {
        log.info("[x] Initialize worker configuration");
        log.info("[x] Worker version: {}", version);

        // reload configuration and tasks
        Tasks tasks = getConfigurationFromServer();

        // refresh credentials
        credentialService.refresh();

        // setup new tasks and triggers
        setupTasks(tasks);

        // setup metrics export
        setupMetricsRegistry();
        addGenericMetrics();

        // export version into graphite
        metricsService.gaugeSingleton(Consts.GAUGE_VERSION, version);

        log.info("[x] Done with configuring worker");
    }

    private void addGenericMetrics() {
        metricsService.gaugeSingleton(Consts.TASKS_COUNT, new AtomicLong(tasksCount));
        metricsService.gaugeSingleton(Consts.CAMERAS_COUNT, new AtomicLong(camerasCount));
    }

    private Tasks getConfigurationFromServer() {
        configuration = configService.getConfiguration(true);
        log.info("[x] Got configuration");

        Tasks tasks = configService.getTasks(true);
        camerasCount = tasks.getCameras().size();
        tasksCount = tasks.getCameras().stream().map(CameraForWorkerDTO::getTasks)
                .mapToLong(Collection::size)
                .sum();
        log.info("[x] Loaded {} cameras and {} tasks", camerasCount, tasksCount);

        // cancel any old tasks and triggers
        log.info("[x] Cancel old tasks");
        try {
            schedulerService.cancelAllTasks();
        } catch (SchedulerException e) {
            log.error("Scheduler exception happens", e);
            return null;
        }

        return tasks;
    }

    @SuppressWarnings("unchecked")
    private void setupTasks(Tasks tasks) {
        if (tasks == null) {
            log.error("No tasks specified for run");
            return;
        }

        try {
            schedulerService.cancelAllTasks();
        } catch (SchedulerException e) {
            log.error("[-] Exception when cancel tasks", e);
        }

        log.info("[x] Setup new tasks");
        tasks.getCameras().forEach(camera -> {
            camera.getTasks().forEach(task -> {
                if(StringUtils.isEmpty(task.getClazz())) {
                    log.error("[-] Null class name for task!");
                    return;
                }

                try {
                    Class<? extends QuartzJobBean> clazz = (Class<? extends QuartzJobBean>) Class.forName(task.getClazz());
                    schedulerService.createTask(clazz, task.getPeriod(), camera);
                } catch (ClassNotFoundException e) {
                    log.error("Class \"{}\" was not found", task.getClazz());
                } catch (SchedulerException e) {
                    log.error("Scheduler exception happens", e);
                }
            });
        });
    }

    private void setupMetricsRegistry() {
        if (configuration.getGraphite() != null) {
            WorkerConfiguration.GraphiteConfiguration graphiteCfg = configuration.getGraphite();

            if (!StringUtils.isEmpty(graphiteCfg.getServerAddress()) && graphiteCfg.getServerPort() != null) {

                log.info("[x] Setup graphite metrics");

                Optional<MeterRegistry> registryOpt = Metrics.globalRegistry.getRegistries().stream()
                        .filter(r -> r instanceof GraphiteMeterRegistry)
                        .findFirst();

                if (registryOpt.isPresent()) {
                    GraphiteMeterRegistry registryConfigured = (GraphiteMeterRegistry) registryOpt.get();

                    GraphiteMeterRegistry registry = new GraphiteMeterRegistry(
                            new GraphiteConfig() {
                                @Override
                                public String get(String s) {
                                    return null;
                                }

                                @Override
                                public String host() {
                                    return graphiteCfg.getServerAddress();
                                }

                                @Override
                                public int port() {
                                    return graphiteCfg.getServerPort();
                                }

                                @Override
                                public Duration step() {
                                    return Duration.ofSeconds(graphiteCfg.getStep());
                                }
                            },
                            Clock.SYSTEM,
                            new GraphiteHierarchicalNameMapper(Consts.TAG),
                            registryConfigured.getDropwizardRegistry()
                    );

                    registryConfigured.stop();

                    Metrics.globalRegistry.remove(registryConfigured);
                    Metrics.addRegistry(registry);
                } else {
                    log.error("No graphite registry found in global registry");
                }
            } else {
                log.error("No server address or server port specified in graphite configuration");
            }
        } else {
            log.error("No graphite configuration specified in worker configuration");
        }
    }
}
