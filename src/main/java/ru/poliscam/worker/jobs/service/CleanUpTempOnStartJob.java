package ru.poliscam.worker.jobs.service;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Component
@Log4j2
public class CleanUpTempOnStartJob {

    @EventListener(ApplicationReadyEvent.class)
    public void cleanUp() throws IOException {
        log.info("[x] Starting cleanup temp");
        File temp = new File("temp/");
        if(!temp.exists()) {
            return;
        }

        for (final File dir : Objects.requireNonNull(temp.listFiles())) {
            FileUtils.forceDelete(dir);
            log.info("[x] Deleting file - {}", dir.getName());
        }


        log.info("[x] Done temp cleanup");
    }
}
