package ru.poliscam.worker.jobs.quartz.parallel;

import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.jobs.quartz.CreateDailyVideoJobBase;

/**
 * @author Nikolay Viguro, 22.10.18
 */

@Log4j2
@Component
public class CreateDailyVideoParallelJob extends CreateDailyVideoJobBase {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        super.executeInternal(jobExecutionContext);
    }

}
