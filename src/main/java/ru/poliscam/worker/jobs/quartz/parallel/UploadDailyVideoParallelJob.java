package ru.poliscam.worker.jobs.quartz.parallel;

import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.jobs.quartz.UploadDailyVideoJobBase;

/**
 * @author Nikolay Viguro, 26.10.18
 */

@Log4j2
@Component
public class UploadDailyVideoParallelJob extends UploadDailyVideoJobBase {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        super.executeInternal(jobExecutionContext);
    }

}
