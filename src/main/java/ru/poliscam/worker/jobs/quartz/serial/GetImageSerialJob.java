package ru.poliscam.worker.jobs.quartz.serial;

import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.jobs.quartz.GetImageJobBase;

import java.util.concurrent.Semaphore;

/**
 * @author Nikolay Viguro, 22.10.18
 */

@Log4j2
@Component
public class GetImageSerialJob extends GetImageJobBase {

    private static final int CONCURRENT_JOBS;
    private static final Semaphore semaphore;

    static {
        //OperatingSystemMXBean sys = ManagementFactory.getOperatingSystemMXBean();
        //CONCURRENT_JOBS = (sys.getAvailableProcessors() - 1) <= 1 ? 1 : sys.getAvailableProcessors() - 1;
        CONCURRENT_JOBS = 2;
        semaphore = new Semaphore(CONCURRENT_JOBS - 1);
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        try {
            semaphore.acquire();
            log.debug("[x] [GetImageSerial] - Available permits: {}", semaphore.availablePermits());
            super.executeInternal(jobExecutionContext);
        } catch (InterruptedException ignored) {
        } finally {
            semaphore.release();
            log.debug("[x] [GetImageSerial] - Released. Available permits: {}", semaphore.availablePermits());
        }
    }

}
