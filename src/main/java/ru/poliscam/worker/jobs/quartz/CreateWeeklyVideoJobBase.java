package ru.poliscam.worker.jobs.quartz;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import lombok.extern.log4j.Log4j2;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.builder.FFmpegOutputBuilder;
import net.bramp.ffmpeg.job.FFmpegJob;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.Utils;
import ru.poliscam.worker.exceptions.FailedToUploadException;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.YoutubeService;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

@Log4j2
@Component
public abstract class CreateWeeklyVideoJobBase extends QuartzJobBean {
    @Autowired
    private ScheduledExecutorService timeoutExecutor;

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Autowired
    private YoutubeService youtubeService;

    @Value("${timeout.video.weekly.video:1800}")
    private Long timeoutVideo;

    @Value("${timeout.video.weekly.music:1800}")
    private Long timeoutMusic;

    @Value("${video.music.file}")
    private String musicFile;

    private String videoDir;
    private FFmpeg ffmpeg;
    private FFprobe ffprobe;
    private FFmpegExecutor executor;
    private String camName;
    private final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

    @Override
    @SuppressWarnings("Duplicates")
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        log.debug("Executing CreateWeeklyVideo with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        CameraForWorkerDTO camera = (CameraForWorkerDTO) jobDataMap.get("camera");

        WorkerConfiguration cfg = configService.getConfiguration(false);
        videoDir = cfg.getVideoDirPath();
        camName = camera.getInternalName();

        Timer timer = metricsService.timer(Consts.TIMER_CREATE_WEEKLY_VIDEO_JOBS, Tag.of("camera", camName));
        Timer timerUpload = metricsService.timer(Consts.TIMER_UPLOAD_WEEKLY_VIDEO_JOBS, Tag.of("camera", camName));

        try {
            ffmpeg = new FFmpeg(cfg.getFfmpegPath());
            ffprobe = new FFprobe(cfg.getFfprobePath());
            executor = new FFmpegExecutor(ffmpeg, ffprobe);

            timer.record(() -> {
                try {
                    boolean success = generateVideo(camera);

                    if(success) {
                        timerUpload.record(() -> {
                            try {
                                log.info("[{}] Starting upload to Youtube", camName);
                                youtubeService.uploadWeek(camera);
                                log.info("[{}] Done upload", camName);
                            } catch (IOException e) {
                                log.error("[{}] Problems with IO", camName, e);
                            } catch (FailedToUploadException e) {
                                log.error("[{}] Failed to upload video to YouTube: {}", camName, e.getMessage());
                            }
                        });
                    }
                } catch (IOException e) {
                    log.error("[{}] Problems with IO", camName, e);
                } catch (InterruptedException e) {
                    log.error("[{}] Interrupted", camName);
                } catch (ExecutionException e) {
                    log.error("[{}] Execution exception", camName, e);
                } catch (TimeoutException e) {
                    log.error("[{}] Timeout", camName);
                }
            });
        } catch (IOException e) {
            log.error("[{}] Problems with IO", camName, e);
        } finally {
            log.info("[{}] Cleanup", camName);
            Utils.deleteTempFile("temp/" + camName + "-week.mp4");
            Utils.deleteTempFile("temp/" + camName + "-week-completed.mp4");
            log.info("[{}] Cleanup done", camName);
        }
    }

    private boolean generateVideo(CameraForWorkerDTO cam) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        FFmpegBuilder builder = ffmpeg.builder();

        // 1. Соберем видео за неделю
        // Найдем видеозаписи за неделю и добавим в input ffmpeg те, которые пройдут проверку ffprobe на ошибки
        List<String> inputs = new ArrayList<>();
        // fixme грязный хак
        final double[] duration = {0.0D};
        for (int day = 0; day <= 6; ++day) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            Date to = calendar.getTime();

            File video = new File(videoDir + "/all/" + cam.getInternalName() + "/video-" + dateFormat.format(to) + ".mp4");

            if (video.exists()) {
                FFmpegProbeResult probe = ffprobe.probe(video.getAbsolutePath());
                if (!probe.hasError()) {
                    inputs.add(video.getAbsolutePath());
                    duration[0] += probe.getFormat().duration * TimeUnit.SECONDS.toNanos(1);
                }
            }
        }

        if (inputs.size() == 0) {
            log.error("[{}] No video files found for concat!", cam.getInternalName());
            return false;
        }

        File tempInputs = Utils.createInputConcatFile(inputs);
        builder.addInput(tempInputs.getAbsolutePath());

        // параметры выходного файла
        FFmpegOutputBuilder outputBuilder = builder.addOutput("temp/" + cam.getInternalName() + "-week.mp4");
        outputBuilder.setVideoCodec("copy");
        outputBuilder.setAudioCodec("copy");
        builder = outputBuilder.done();

        // формат - сборка
        builder.setFormat("concat");

        // какая то неведомая хуйня для сборки
        builder.addExtraArgs("-safe", "0");

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        log.info("[{}] Concat week video...", cam.getInternalName());

        // запускаем FFMPEG
        FFmpegJob job;
        Future results = null;
        try {
            job = executor.createJob(builder, Utils.createProgressListener("[" + cam.getInternalName() + "] Concating week", duration[0]));
            results = timeoutExecutor.submit(job);
            results.get(timeoutVideo, TimeUnit.SECONDS);

            if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                job.interrupt();
                results.cancel(true);
                log.error("[{}] Week concat interrupted by timeout", camName);
                return false;
            }
        } finally {
            if(results != null) {
                results.cancel(true);
            }
            Utils.deleteTempFile(tempInputs.getAbsolutePath());
        }

        log.info("[{}] Done concating", cam.getInternalName());
        log.info("[{}] Start adding music...", cam.getInternalName());

        // 2. Теперь мы наложим музыку на весь файл
        builder = ffmpeg.builder();
        builder.addInput("temp/" + cam.getInternalName() + "-week.mp4");
        builder.addInput(musicFile);

        // параметры выходного файла
        outputBuilder = builder.addOutput("temp/" + cam.getInternalName() + "-week-completed.mp4");
        outputBuilder.setStrict(FFmpegBuilder.Strict.EXPERIMENTAL);
        outputBuilder.addExtraArgs("-map", "0:v");
        outputBuilder.addExtraArgs("-map", "1:a");
        outputBuilder.addExtraArgs("-shortest");
        outputBuilder.setVideoCodec("copy");
        outputBuilder.setAudioCodec("copy");
        builder = outputBuilder.done();

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        // запускаем FFMPEG и ждем 1ч
        try {
            job = executor.createJob(builder);
            results = timeoutExecutor.submit(job);
            results.get(timeoutMusic, TimeUnit.SECONDS);

            if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                job.interrupt();
                results.cancel(true);
                log.error("[{}] Week concat (add music step) interrupted by timeout", camName);
                return false;
            }
        } finally {
            results.cancel(true);
        }

        log.info("[{}] Done adding music", cam.getInternalName());
        return true;
    }
}
