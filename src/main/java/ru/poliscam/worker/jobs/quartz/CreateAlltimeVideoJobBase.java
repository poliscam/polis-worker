package ru.poliscam.worker.jobs.quartz;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import lombok.extern.log4j.Log4j2;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.builder.FFmpegOutputBuilder;
import net.bramp.ffmpeg.job.FFmpegJob;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import org.apache.logging.log4j.core.util.FileUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.Utils;
import ru.poliscam.worker.exceptions.FailedToUploadException;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.YoutubeService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.*;
import java.util.stream.Stream;

@Log4j2
@Component
public abstract class CreateAlltimeVideoJobBase extends QuartzJobBean {
    @Autowired
    private ScheduledExecutorService timeoutExecutor;

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Autowired
    private YoutubeService youtubeService;

    @Value("${timeout.video.alltime.video:1800}")
    private Long timeoutVideo;

    @Value("${timeout.video.alltime.music:1800}")
    private Long timeoutMusic;

    @Value("${video.music.file}")
    private String musicFile;

    private String videoDir;
    private FFmpeg ffmpeg;
    private FFprobe ffprobe;
    private FFmpegExecutor executor;
    private String camName;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

    @Override
    @SuppressWarnings("Duplicates")
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        log.debug("Executing CreateAlltimeVideo with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        CameraForWorkerDTO camera = (CameraForWorkerDTO) jobDataMap.get("camera");

        WorkerConfiguration cfg = configService.getConfiguration(false);
        videoDir = cfg.getVideoDirPath();
        camName = camera.getInternalName();

        Timer timer = metricsService.timer(Consts.TIMER_CREATE_ALLTIME_VIDEO_JOBS, Tag.of("camera", camName));
        Timer timerUpload = metricsService.timer(Consts.TIMER_UPLOAD_ALLTIME_VIDEO_JOBS, Tag.of("camera", camName));

        try {
            ffmpeg = new FFmpeg(cfg.getFfmpegPath());
            ffprobe = new FFprobe(cfg.getFfprobePath());
            executor = new FFmpegExecutor(ffmpeg, ffprobe);

            timer.record(() -> {
                try {
                    boolean success = generateVideo(camera);

                    if(success) {
                        timerUpload.record(() -> {
                            try {
                                log.info("[{}] Starting upload to Youtube", camName);
                                youtubeService.uploadAllTime(camera);
                                log.info("[{}] Done upload", camName);
                            } catch (IOException e) {
                                log.error("[{}] Problems with IO", camName, e);
                            } catch (FailedToUploadException e) {
                                log.error("[{}] Failed to upload video to YouTube: {}", camName, e.getMessage());
                            }
                        });
                    }
                } catch (IOException e) {
                    log.error("[{}] Problems with IO", camName, e);
                } catch (InterruptedException e) {
                    log.error("[{}] Interrupted", camName);
                } catch (ExecutionException e) {
                    log.error("[{}] Execution exception", camName, e);
                } catch (TimeoutException e) {
                    log.error("[{}] Timeout", camName);
                }
            });
        } catch (IOException e) {
            log.error("[{}] Problems with IO", camName, e);
        } finally {
            log.info("[{}] Cleanup", camName);
            Utils.deleteTempFile("temp/" + camName + "-fast-daily.mp4");
            Utils.deleteTempFile("temp/" + camName + "-fast.mp4");
            Utils.deleteTempFile("temp/" + camName + "-fast-completed.mp4");
            log.info("[{}] Cleanup done", camName);
        }
    }

    private boolean generateVideo(CameraForWorkerDTO cam) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        // создадим нужные папки, если их нет
        File all = new File(videoDir + "/all/" + cam.getInternalName());
        FileUtils.mkdir(all, true);

        Date date = new Date();
        FFmpegBuilder builder = ffmpeg.builder();

        File dailyVideo = new File(videoDir + "/" + cam.getInternalName() + "/daily.mp4");
        if (!dailyVideo.exists() || dailyVideo.isDirectory()) {
            log.info("[{}] Daily file does not exists... Exiting", cam.getInternalName());
            return false;
        }

        FFmpegProbeResult probeResult = ffprobe.probe(dailyVideo.getAbsolutePath());
        if(probeResult.hasError()) {
            log.error("[{}] Something wrong with daily file: {}", cam.getInternalName(), probeResult.getError());
            return false;
        }

        // 1. приведем дневной файл в "ускоренную" форму
        // параметры выходного файла
        FFmpegOutputBuilder outputBuilder = builder.addOutput("temp/" + cam.getInternalName() + "-fast-daily.mp4");
        outputBuilder.setVideoCodec("libx264");
        outputBuilder.setVideoFilter("scale=800:trunc(ow/a/2)*2,setpts=0.1*PTS");
        outputBuilder.setStrict(FFmpegBuilder.Strict.EXPERIMENTAL);
        outputBuilder.setVideoFrameRate(15);
        builder = outputBuilder.done();

        // исходный файл - daily
        builder.addInput(probeResult);

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        log.info("[{}] Create observer video...", cam.getInternalName());

        // запускаем FFMPEG и ждем 1ч
        // fixme грязный хак
        final double[] duration = {0.0D};
        FFmpegJob job = executor.createJob(builder, Utils.createProgressListener("[" + cam.getInternalName() + "] Make fast daily", duration[0]));
        Future results = timeoutExecutor.submit(job);
        results.get(timeoutVideo, TimeUnit.SECONDS);

        if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
            job.interrupt();
            results.cancel(true);
            log.error("[{}] Encode interrupted by timeout", camName);
            return false;
        }

        log.info("[{}] Done with creating observer video...", cam.getInternalName());

        // 2. теперь соберем все в кучу
        // создадим новый билдер
        log.info("[{}] Concat observer video...", cam.getInternalName());
        builder = ffmpeg.builder();

        // скопируем конвертированный файл в директорию к остальным таким же на SDN
        File fastDaily = new File("temp/" + cam.getInternalName() + "-fast-daily.mp4");
        File fastDailyOnSDN = new File(videoDir + "/all/" + cam.getInternalName() + "/video-" + dateFormat.format(date) + ".mp4");
        log.info("[{}] Copy fast video to CDN... ({})", cam.getInternalName(), fastDailyOnSDN.getAbsolutePath());

        Files.copy(fastDaily.toPath(), fastDailyOnSDN.toPath(), StandardCopyOption.REPLACE_EXISTING);

        log.info("[{}] Done copying...", cam.getInternalName());

        // Найдем видеозаписи и добавим в input ffmpeg те, которые пройдут проверку ffprobe на ошибки
        log.info("[{}] Checking files to concat...", cam.getInternalName());
        Set<String> inputs = new TreeSet<>();

        // fixme грязный хак
        final double[] duration2 = {0.0D};

        try (Stream<Path> stream = Files.find(Paths.get(all.getAbsolutePath()),
                Integer.MAX_VALUE, (fPath, attr) ->
                        String.valueOf(fPath).endsWith(".mp4"))) {
            stream.sorted().forEach(
                    file -> {
                        try {
                            FFmpegProbeResult probe = ffprobe.probe(file.toFile().getAbsolutePath());
                            if (!probe.hasError()) {
                                inputs.add(file.toFile().getAbsolutePath());
                                duration2[0] += probe.getFormat().duration * TimeUnit.SECONDS.toNanos(1);
                            }
                        } catch (IOException e) {
                            //ignored
                        }
                    });
        }
        log.info("[{}] Done checking files to concat...", cam.getInternalName());
        log.info("[{}] Сoncating...", cam.getInternalName());

        File tempInputs = Utils.createInputConcatFile(inputs);

        builder.addInput(tempInputs.getAbsolutePath());

        // параметры выходного файла
        outputBuilder = builder.addOutput("temp/" + cam.getInternalName() + "-fast.mp4");
        outputBuilder.setVideoCodec("copy");
        outputBuilder.setAudioCodec("copy");
        builder = outputBuilder.done();

        // формат - сборка
        builder.setFormat("concat");

        // какая то неведомая хуйня для сборки
        builder.addExtraArgs("-safe", "0");

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        try {
            // запускаем FFMPEG и ждем 1ч
            job = executor.createJob(builder, Utils.createProgressListener("[" + cam.getInternalName() + "] Concating", duration2[0]));
            results = timeoutExecutor.submit(job);
            results.get(timeoutVideo, TimeUnit.SECONDS);

            if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                job.interrupt();
                results.cancel(true);
                log.error("[{}] Encode interrupted by timeout", camName);
                return false;
            }
        } finally {
            Utils.deleteTempFile(tempInputs.getAbsolutePath());
        }

        log.info("[{}] Done concating!", cam.getInternalName());
        log.info("[{}] Start adding music...", cam.getInternalName());

        // 3. Теперь мы наложим музыку на весь файл
        builder = ffmpeg.builder();
        builder.addInput("temp/" + cam.getInternalName() + "-fast.mp4");
        builder.addInput(musicFile);

        // параметры выходного файла
        outputBuilder = builder.addOutput("temp/" + cam.getInternalName() + "-fast-completed.mp4");
        outputBuilder.setStrict(FFmpegBuilder.Strict.EXPERIMENTAL);
        outputBuilder.addExtraArgs("-map", "0:v");
        outputBuilder.addExtraArgs("-map", "1:a");
        outputBuilder.addExtraArgs("-shortest");
        outputBuilder.setVideoCodec("copy");
        outputBuilder.setAudioCodec("copy");
        builder = outputBuilder.done();

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        // запускаем FFMPEG и ждем 1ч
        try {
            job = executor.createJob(builder);
            results = timeoutExecutor.submit(job);
            results.get(timeoutMusic, TimeUnit.SECONDS);

            if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                job.interrupt();
                results.cancel(true);
                log.error("[{}] Week concat (add music step) interrupted by timeout", camName);
                return false;
            }
        } finally {
            results.cancel(true);
        }

        log.info("[{}] Done adding music!", cam.getInternalName());
        return true;
    }
}
