package ru.poliscam.worker.jobs.quartz;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import lombok.extern.log4j.Log4j2;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.exceptions.FailedToUploadException;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.YoutubeService;

import java.io.IOException;

@Log4j2
@Component
public abstract class UploadDailyVideoJobBase extends QuartzJobBean {
    @Autowired
    private MetricsService metricsService;

    @Autowired
    private YoutubeService youtubeService;

    private String camName;

    @Override
    @SuppressWarnings("Duplicates")
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        log.debug("Executing UploadDailyVideo with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        CameraForWorkerDTO camera = (CameraForWorkerDTO) jobDataMap.get("camera");

        camName = camera.getInternalName();
        Timer timerUpload = metricsService.timer(Consts.TIMER_UPLOAD_DAILY_VIDEO_JOBS, Tag.of("camera", camName));

        timerUpload.record(() -> {
            try {
                log.info("[{}] Starting upload to Youtube", camName);
                youtubeService.uploadDaily(camera);
                log.info("[{}] Done upload", camName);
            } catch (IOException e) {
                log.error("[{}] Problems with IO", camName, e);
            } catch (FailedToUploadException e) {
                log.error("[{}] Failed to upload video to YouTube: {}", camName, e.getMessage());
            }
        });
    }
}
