package ru.poliscam.worker.jobs.quartz;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import lombok.extern.log4j.Log4j2;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.builder.FFmpegOutputBuilder;
import net.bramp.ffmpeg.exception.http.*;
import net.bramp.ffmpeg.job.FFmpegJob;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import org.apache.logging.log4j.core.util.FileUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.Utils;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.MetricsService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.*;
import java.util.stream.Stream;

@Log4j2
@Component
public abstract class CreateDailyVideoJobBase extends QuartzJobBean {
    @Autowired
    private ScheduledExecutorService timeoutExecutor;

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Value("${timeout.video.daily:1800}")
    private Long timeout;

    private String videoDir;
    private FFmpeg ffmpeg;
    private FFprobe ffprobe;
    private FFmpegExecutor executor;
    private String camName;
    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    @Override
    @SuppressWarnings("Duplicates")
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        log.debug("Executing CreateDailyVideo with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        CameraForWorkerDTO camera = (CameraForWorkerDTO) jobDataMap.get("camera");

        WorkerConfiguration cfg = configService.getConfiguration(false);
        videoDir = cfg.getVideoDirPath();

        try {
            camName = camera.getInternalName();

            Timer timer = metricsService.timer(Consts.TIMER_CREATE_DAILY_VIDEO_JOBS, Tag.of("camera", camName));

            ffmpeg = new FFmpeg(cfg.getFfmpegPath());
            ffprobe = new FFprobe(cfg.getFfprobePath());
            executor = new FFmpegExecutor(ffmpeg, ffprobe);

            timer.record(() -> {
                try {
                    generateVideo(camera);
                } catch (TimeoutException e) {
                    log.error("[{}] Timeout", camName);
                } catch (IOException e) {
                    log.error("[{}] Problems with IO", camName, e);
                } catch (InterruptedException e) {
                    log.error("[{}] Interrupted", camName);
                } catch (ExecutionException e) {
                    Throwable t = e.getCause();
                    if(t instanceof BadRequestException) {
                        log.error("[{}] BadRequest", camName);
                    } else if(t instanceof ForbiddenRequestException) {
                        log.error("[{}] Forbidden", camName);
                    } else if(t instanceof Generic40XException) {
                        log.error("[{}] Generic40X", camName);
                    } else if(t instanceof NotFoundException) {
                        log.error("[{}] NotFound", camName);
                    } else if(t instanceof ServerErrorException) {
                        log.error("[{}] ServerError", camName);
                    } else if(t instanceof UnauthorizedRequestException) {
                        log.error("[{}] Unauthorized", camName);
                    } else {
                        log.error("[{}] Execution exception", camName, e);
                    }
                }
            });
        } catch (IOException e) {
            log.error("[{}] Problems with IO", camName, e);
        }
    }

    private boolean generateVideo(CameraForWorkerDTO cam) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        final File path = new File("images/" + camName + "/" + dateFormat.format(new Date()) + "/");
        File dailyVideo = new File(videoDir + "/" + camName + "/daily.mp4");
        FFmpegBuilder builder = ffmpeg.builder();

        // создадим директории, если их нет
        FileUtils.mkdir(path, true);
        FileUtils.mkdir(new File(videoDir + "/" + camName), true);

        // Удаляем старое видео в любом случае, т.к. иначе это ломает статистику по камерам
        Files.deleteIfExists(dailyVideo.toPath());

        // Найдем все картинке в папке камеры за сегодня и добавим в input ffmpeg
        Set<String> inputs = new TreeSet<>();
        try (Stream<Path> stream = Files.find(path.toPath(),
                Integer.MAX_VALUE, (fPath, attr) ->
                        String.valueOf(fPath).endsWith(".jpg"))) {
            stream.sorted().forEach(
                    file -> inputs.add(file.toFile().getAbsolutePath())
            );
        }

        // Нету фреймрейта для входящих файлов :(
        builder.addExtraArgs("-r", "5");

        // Создаем темповый файл, в котором будут все файлы на вход
        if (inputs.size() == 0) {
            log.error("[{}] No images found to create daily video!", camName);
            return false;
        }
        File tempInput = Utils.createInputConcatFile(inputs);
        builder.addInput(tempInput.getAbsolutePath());

        // параметры выходного файла
        FFmpegOutputBuilder outputBuilder = builder.addOutput("temp/" + camName + "-daily.mp4");
        outputBuilder.setVideoCodec("libx264");
        outputBuilder.setVideoFilter("scale=" + cam.getWidth() + ":trunc(ow/a/2)*2,fps=5");
        outputBuilder.setStrict(FFmpegBuilder.Strict.EXPERIMENTAL);
        builder = outputBuilder.done();

        // формат - сборка
        builder.setFormat("concat");

        // какая то неведомая хуйня для сборки
        builder.addExtraArgs("-safe", "0");

        // перезаписываем файлы
        builder.overrideOutputFiles(true);

        log.info("[{}] Create hourly video...", camName);

        // запускаем FFMPEG
        File tempVideo = null;
        Future results = null;
        try {
            FFmpegJob job = executor.createJob(builder);
            results = timeoutExecutor.submit(job);
            results.get(timeout, TimeUnit.SECONDS);

            if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                job.interrupt();
                results.cancel(true);
                log.error("[{}] Encode interrupted by timeout", camName);
                return false;
            }

            log.info("[{}] Done encode", camName);

            FFmpegProbeResult completedProbe = ffprobe.probe("temp/" + camName + "-daily.mp4");
            if (completedProbe.hasError()) {
                log.error("[{}] Something wrong with tmp daily file: {}", cam.getInternalName(), completedProbe.getError());
                return false;
            } else {
                log.info("[{}] Duration of daily file: {}", cam.getInternalName(), completedProbe.getFormat().duration);
            }

            // скопируем конвертированный файл в директорию на SDN
            tempVideo = new File("temp/" + camName + "-daily.mp4");
            Files.copy(tempVideo.toPath(), dailyVideo.toPath(), StandardCopyOption.REPLACE_EXISTING);

            log.info("[{}] Done copying...", cam.getInternalName());
            return true;
        } finally {
            // Чистим за собой
            if(results != null) {
                results.cancel(true);
            }
            Utils.deleteTempFile(tempInput.getAbsolutePath());
            if(tempVideo != null) {
                Utils.deleteTempFile(tempVideo.getAbsolutePath());
            }
        }
    }
}
