package ru.poliscam.worker.jobs.quartz;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import lombok.extern.log4j.Log4j2;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.builder.FFmpegOutputBuilder;
import net.bramp.ffmpeg.exception.http.*;
import net.bramp.ffmpeg.job.FFmpegJob;
import org.apache.logging.log4j.core.util.FileUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import ru.poliscam.worker.Consts;
import ru.poliscam.worker.configs.RabbitMqConfig;
import ru.poliscam.worker.models.CameraForWorkerDTO;
import ru.poliscam.worker.models.ModificationResult;
import ru.poliscam.worker.models.StatusMessage;
import ru.poliscam.worker.models.WorkerConfiguration;
import ru.poliscam.worker.services.ConfigService;
import ru.poliscam.worker.services.MetricsService;
import ru.poliscam.worker.services.ScriptService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
@Component
public abstract class GetImageJobBase extends QuartzJobBean {
    @Autowired
    private ScheduledExecutorService timeoutExecutor;

    @Value("${timeout.image.get:100}")
    private Long timeout;

    @Value("${worker.id}")
    private Long worker;

    @Value("${video.font}")
    private String font;

    @Autowired
    private AmqpTemplate template;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricsService metricsService;

    @Autowired
    private ScriptService scriptService;

    private FFmpeg ffmpeg;
    private FFmpegExecutor executor;
    private String camName;
    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    private DateFormat timeFormat = new SimpleDateFormat("HH-mm", Locale.ENGLISH);
    private Date date = new Date();

    @Override
    @SuppressWarnings("Duplicates")
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        log.debug("Executing GetImage with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        CameraForWorkerDTO camera = (CameraForWorkerDTO) jobDataMap.get("camera");

        WorkerConfiguration cfg = configService.getConfiguration(false);
        camName = camera.getInternalName();

        AtomicBoolean failed = metricsService.bool(
                Consts.GAUGE_FAILED_GET_IMG_JOBS,
                false,
                Tag.of("camera", camName)
        );

        Timer timer = metricsService.timer(Consts.TIMER_GET_IMG_JOBS, Tag.of("camera", camName));

        try {
            ffmpeg = new FFmpeg(cfg.getFfmpegPath());
            FFprobe ffprobe = new FFprobe(cfg.getFfprobePath());
            executor = new FFmpegExecutor(ffmpeg, ffprobe);

            timer.record(() -> {
                try {
                    getImage(camera);
                    failed.set(false);
                } catch (TimeoutException e) {
                    log.error("[{}] Timeout", camName);
                    failed.set(true);
                } catch (IOException e) {
                    log.error("[{}] Problems with IO", camName, e);
                    failed.set(true);
                } catch (InterruptedException e) {
                    log.error("[{}] Interrupted", camName);
                    failed.set(true);
                } catch (ExecutionException e) {
                    Throwable t = e.getCause();
                    if(t instanceof BadRequestException) {
                        log.error("[{}] BadRequest", camName);
                    } else if(t instanceof ForbiddenRequestException) {
                        log.error("[{}] Forbidden", camName);
                    } else if(t instanceof Generic40XException) {
                        log.error("[{}] Generic40X", camName);
                    } else if(t instanceof NotFoundException) {
                        log.error("[{}] NotFound", camName);
                    } else if(t instanceof ServerErrorException) {
                        log.error("[{}] ServerError", camName);
                    } else if(t instanceof UnauthorizedRequestException) {
                        log.error("[{}] Unauthorized", camName);
                    } else if(t instanceof ConnectionTimedOutException) {
                        log.error("[{}] Connection timeout", camName);
                    } else if(t instanceof ConnectionRefusedException) {
                        log.error("[{}] Connection refused", camName);
                    } else {
                        log.error("[{}] Execution exception", camName, e);
                    }

                    failed.set(true);
                }
            });
        } catch (IOException e) {
            log.error("[{}] Problems with IO", camName, e);
            failed.set(true);
        }

        template.convertAndSend(RabbitMqConfig.EXCHANGE,
                RabbitMqConfig.STATUS_KEY,
                StatusMessage.builder()
                        .worker(worker)
                        .cameraId(camera.getId())
                        .status(failed.get() ? StatusMessage.Status.IMAGE_NOT_FETCHED : StatusMessage.Status.IMAGE_FETCHED)
                        .build());
    }

    private void getImage(CameraForWorkerDTO cam)
            throws IOException, InterruptedException, ExecutionException, TimeoutException {
        FFmpegBuilder builder = ffmpeg.builder();
        String url = cam.getRealtimeUrl();
        boolean uaOverriden = false;

        if(!StringUtils.isEmpty(cam.getFfmpegScript())) {
            log.info("[{}] Run modification script", camName);
            ModificationResult result = scriptService.runScript(cam);

            if(result != null) {
                if(!StringUtils.isEmpty(result.getUrl())) {
                    log.debug("[{}] Set custom URL: {}", camName, result.getUrl());
                    url = result.getUrl();
                }
                if(!CollectionUtils.isEmpty(result.getParams())) {
                    for(Map.Entry<String, String> entry : result.getParams().entrySet()) {
                        if(entry.getKey().equals("useragent")) {
                            log.debug("[{}] Set custom UA: {}", camName, entry.getValue());
                            builder.setUserAgent(entry.getValue());
                            uaOverriden = true;
                        } else {
                            log.debug("[{}] Set custom extra param: {} -> {}", camName, entry.getKey(), entry.getValue());
                            builder.addExtraArgs(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }
        }

        if(StringUtils.isEmpty(url) || StringUtils.isEmpty(url.trim())) {
            log.error("[{}] No URL to get image passed", camName);
            return;
        }

        final File path = new File("images/" + cam.getInternalName() + "/" + dateFormat.format(date) + "/" + timeFormat.format(date));
        final File fullpath = new File(path.getAbsolutePath() + "/cam-" + System.currentTimeMillis() + ".jpg");

        // создадим директории, если их нет
        FileUtils.mkdir(path, true);

        log.info("[{}] Getting image...", camName);

        // если у нас картинка дергается просто
        if (cam.getUseCurl()) {
            byte[] imageBytes = restTemplate.getForObject(url, byte[].class);
            if (imageBytes != null) {
                Files.write(fullpath.toPath(), imageBytes);
            } else {
                log.error("[{}] Error happens when trying to get image", camName);
                return;
            }
        } else {
            FFmpegOutputBuilder outputBuilder = builder.addOutput(fullpath.getAbsolutePath());

            // нам нужен только один фрейм
            outputBuilder.setFrames(1);

            // добавляем видео-фильтр с текстом PolisCam.Ru и временем
            outputBuilder.setVideoFilter("drawtext=fontfile=" + font + ": text='PolisCam.Ru %{localtime}': x=(w-tw)/2: y=h-(2*lh): fontcolor=white: box=1: boxcolor=0x00000000@1");

            // сохраняем в картинку
            outputBuilder.setFormat("image2");

            // Возвращаемся к FFMPEG билдеру
            builder = outputBuilder.done();

            // если у нас не RTMP, добавляем UserAgent от Хрома и таймаут
            if (!url.startsWith("rtmp")) {
                if(!uaOverriden) {
                    builder.setUserAgent("Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
                }
                builder.addExtraArgs("-timeout", "60000000");

                // input
                builder.addInput(url);
            } else {
                // input rtmp (ffmpeg собран с librtmp!), в нативном нет поддержки таймаута О_О
                // из-за чего плодится куча висящих процессов
                builder.addInput(url + " live=1 timeout=100");
            }
            // запускаем FFMPEG

            log.debug("[{}] Run ffmpeg", camName);

            Future results = null;
            try {
                FFmpegJob job = executor.createJob(builder);
                results = timeoutExecutor.submit(job);
                results.get(timeout, TimeUnit.SECONDS);

                log.debug("[{}] Done running ffmpeg. Job state is {}", camName, job.getState());

                if (!job.getState().equals(FFmpegJob.State.FINISHED) && !job.getState().equals(FFmpegJob.State.INTERRUPTED) && !job.getState().equals(FFmpegJob.State.FAILED)) {
                    log.debug("[{}] Interrupting", camName);
                    job.interrupt();
                    results.cancel(true);
                    log.debug("[{}] Interrupting ok", camName);
                }
            } finally {
                if(results != null) {
                    results.cancel(true);
                }
            }
        }


        if (!fullpath.exists()) {
            log.error("[{}] Image NOT fetched! :(", camName);
        } else {
            log.info("[{}] Image fetched! :)", camName);
        }

        log.debug("[{}] Done getting image", camName);
    }
}
