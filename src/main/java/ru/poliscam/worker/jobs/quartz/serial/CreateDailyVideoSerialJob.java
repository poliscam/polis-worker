package ru.poliscam.worker.jobs.quartz.serial;

import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.jobs.quartz.CreateDailyVideoJobBase;

import java.util.concurrent.Semaphore;

/**
 * @author Nikolay Viguro, 22.10.18
 */

@Log4j2
@Component
public class CreateDailyVideoSerialJob extends CreateDailyVideoJobBase {

    private static final int CONCURRENT_JOBS;
    private static final Semaphore semaphore;

    static {
        //OperatingSystemMXBean sys = ManagementFactory.getOperatingSystemMXBean();
        //CONCURRENT_JOBS = (sys.getAvailableProcessors() - 1) <= 1 ? 1 : sys.getAvailableProcessors() - 1;
        CONCURRENT_JOBS = 2;
        semaphore = new Semaphore(CONCURRENT_JOBS - 1);
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        try {
            semaphore.acquire();
            log.debug("[x] [CreateDailyVideo] - Available permits: {}", semaphore.availablePermits());
            super.executeInternal(jobExecutionContext);
        } catch (InterruptedException ignored) {
        } finally {
            semaphore.release();
            log.debug("[x] [CreateDailyVideo] - Released. Available permits: {}", semaphore.availablePermits());
        }
    }

}
