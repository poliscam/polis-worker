package ru.poliscam.worker.exceptions;

/**
 * @author Nikolay Viguro, 22.10.18
 */
public class FailedToUploadException extends Exception {
    public FailedToUploadException() {
        super();
    }

    public FailedToUploadException(String s) {
        super(s);
    }
}
