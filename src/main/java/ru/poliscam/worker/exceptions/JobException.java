package ru.poliscam.worker.exceptions;

/**
 * @author Nikolay Viguro, 22.10.18
 */
public class JobException extends IllegalStateException {
    public JobException() {
        super();
    }

    public JobException(String s) {
        super(s);
    }
}
