package ru.poliscam.worker.exceptions;

/**
 * @author Nikolay Viguro, 22.10.18
 */
public class NoMoreCredentialsException extends Exception {
    public NoMoreCredentialsException() {
        super();
    }

    public NoMoreCredentialsException(String s) {
        super(s);
    }
}
