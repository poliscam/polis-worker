package ru.poliscam.worker.exceptions;

/**
 * @author Nikolay Viguro, 22.10.18
 */
public class LimitExceededException extends Exception {
    public LimitExceededException() {
        super();
    }

    public LimitExceededException(String s) {
        super(s);
    }
}
