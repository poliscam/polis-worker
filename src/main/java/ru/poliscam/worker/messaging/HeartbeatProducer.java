package ru.poliscam.worker.messaging;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.worker.configs.RabbitMqConfig;
import ru.poliscam.worker.models.HeartbeatMessage;
import ru.poliscam.worker.services.ConfigService;

@Component
@Log4j2
public class HeartbeatProducer {
    @Value("${worker.id}")
    private Long worker;

    @Autowired
    private ConfigService configService;

    @Autowired
    private AmqpTemplate template;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(fixedDelay = 10_000, initialDelay = 60_000)
    public void produceHeartbeat() {
        if(configService.initialized()) {
            log.info("[x] Send heartbeat to server...");
            template.convertAndSend(RabbitMqConfig.QUEUE_HEARTBEAT,
                    HeartbeatMessage.builder()
                            .identifier(worker)
                            .ip(configService.getIp())
                            .version(configService.getVersion())
                            .build());
        }
    }
}
