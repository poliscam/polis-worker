package ru.poliscam.worker.messaging;

import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.quartz.SchedulerException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.poliscam.worker.configs.RabbitMqConfig;
import ru.poliscam.worker.jobs.service.InitializeJob;
import ru.poliscam.worker.models.ControlMessage;
import ru.poliscam.worker.services.SchedulerService;

import java.io.IOException;
import java.nio.file.*;

import static org.springframework.amqp.support.AmqpHeaders.DELIVERY_TAG;

@Component
@Log4j2
public class ControlConsumer {
    @Value("${worker.id}")
    private Long worker;

    @Value("${restart.command}")
    private String restartCommand;

    @Value("${update.dir}")
    private String updateDir;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private InitializeJob initializeJob;

    @SuppressWarnings("unchecked")
    @RabbitListener(queues = RabbitMqConfig.QUEUE_CONTROL, containerFactory = "manualContainerFactory")
    public void processMessage(ControlMessage message, Channel channel, @Header(DELIVERY_TAG) long tag) throws IOException {
        if (message != null && message.getIdentifier() != null) {
            if (message.getIdentifier().equals(worker)) {
                try {
                    log.info("[x] Control message received");

                    if(!CollectionUtils.isEmpty(message.getCommands())) {
                        message.getCommands().forEach(command -> {
                            switch (command.getType()) {
                                case RELOAD_WORKER: {
                                    log.info("[x] Got reload command");
                                    initializeJob.reloadData();
                                    break;
                                }
                                case RUN_TASK: {
                                    log.info("[x] Got run task command");
                                    if(!StringUtils.isEmpty(command.getClazz()) && command.getCamera() != null) {
                                        try {
                                            Class<? extends QuartzJobBean> clazz = (Class<? extends QuartzJobBean>) Class.forName(
                                                    command.getClazz()
                                            );

                                            schedulerService.runTaskNow(clazz, command.getCamera());
                                        } catch (ClassNotFoundException e) {
                                            log.error("[-] Class \"{}\" was not found", command.getClazz());
                                        } catch (SchedulerException e) {
                                            log.error("[-] Scheduler exception happens", e);
                                        }
                                    } else {
                                        log.error("[-] Data is empty or incomplete for RUN_TASK job!");
                                    }
                                    break;
                                }
                                case REMOVE_TASK: {
                                    log.info("[x] Got remove task command");
                                    if(!StringUtils.isEmpty(command.getClazz()) && command.getCamera() != null) {
                                        try {
                                            Class<? extends QuartzJobBean> clazz = (Class<? extends QuartzJobBean>) Class.forName(
                                                    command.getClazz()
                                            );

                                            schedulerService.cancelTask(clazz, command.getCamera());
                                        } catch (ClassNotFoundException e) {
                                            log.error("[-] Class \"{}\" was not found", command.getClazz());
                                        } catch (SchedulerException e) {
                                            log.error("[-] Scheduler exception happens", e);
                                        }
                                    } else {
                                        log.error("[-] Data is empty or incomplete for REMOVE_TASK job!");
                                    }
                                    break;
                                }
                                case ADD_TASK: {
                                    log.info("[x] Got add task command");
                                    if(!StringUtils.isEmpty(command.getClazz()) && command.getCamera() != null
                                    && !CollectionUtils.isEmpty(command.getData()) && command.getData().get("cron") != null) {
                                        try {
                                            Class<? extends QuartzJobBean> clazz = (Class<? extends QuartzJobBean>) Class.forName(
                                                    command.getClazz()
                                            );

                                            schedulerService.createTask(
                                                    clazz,
                                                    command.getData().get("cron"),
                                                    command.getCamera()
                                            );
                                        } catch (ClassNotFoundException e) {
                                            log.error("[-] Class \"{}\" was not found", command.getClazz());
                                        } catch (SchedulerException e) {
                                            log.error("[-] Scheduler exception happens", e);
                                        }
                                    } else {
                                        log.error("[-] Data is empty or incomplete for ADD_TASK job!");
                                    }
                                    break;
                                }
                                case RESTART: {
                                    log.info("[x] Got restart command");
                                    try {
                                        Runtime.getRuntime().exec(restartCommand);
                                    } catch (IOException e) {
                                        log.error("[-] Can't restart worker", e);
                                    }
                                    break;
                                }
                                case UPDATE: {
                                    log.info("[x] Got update command");
                                    Path currentDirectory = Paths.get("./worker.jar").toAbsolutePath().normalize();
                                    try {
                                        Files.copy(
                                                Paths.get(updateDir + "/worker.jar"),
                                                currentDirectory,
                                                StandardCopyOption.REPLACE_EXISTING);
                                        Runtime.getRuntime().exec(restartCommand);
                                    } catch (NoSuchFileException e) {
                                        log.error("[-] No file found for update: {}", e.getMessage());
                                    } catch (IOException e) {
                                        log.error("[-] Can't update worker jar to new version", e);
                                    }
                                    break;
                                }
                                default: {
                                    log.error("[-] Unknown command: {}", command.getType());
                                }
                            }
                        });
                    } else {
                        log.error("[-] No command list in command message!");
                    }

                    channel.basicAck(tag, false);
                    log.info("[x] Control message processed");
                } catch (Exception e) {
                    log.error("[x] Something went wrong with processing the control message: {}", message, e);
                    channel.basicNack(tag, false, false);
                }
            } else {
                log.debug("[-] Control message for worker #{}", message.getIdentifier());
                channel.basicNack(tag, false, true);
            }
        } else {
            log.error("[-] Control message is empty!");
            channel.basicNack(tag, false, false);
        }
    }
}
