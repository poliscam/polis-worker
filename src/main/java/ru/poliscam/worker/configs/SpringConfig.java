package ru.poliscam.worker.configs;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@EnableScheduling
@EnableAsync
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SpringConfig {
    private static final int MAXIMUM_THREADS = 100;

    @Bean
    public RestTemplate restTemplate(List<HttpMessageConverter<?>> messageConverters, RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(30_000)
                .setReadTimeout(30_000)
                .additionalMessageConverters(messageConverters)
           .build();
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        return new ByteArrayHttpMessageConverter();
    }

    @Bean
    public Executor taskExecutor() {
        return Executors.newFixedThreadPool(MAXIMUM_THREADS);
    }

    @Bean
    public ScheduledExecutorService timeoutExecutor() {
        return Executors.newScheduledThreadPool(MAXIMUM_THREADS);
    }
}