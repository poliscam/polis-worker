package ru.poliscam.worker.aspectes;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.poliscam.worker.annotations.SuccessGauge;
import ru.poliscam.worker.services.MetricsService;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nikolay Viguro, 22.10.18
 */

@Log4j2
@Aspect
@Component
public class CountSuccessAspect {
    @Autowired
    private MetricsService metricsService;

    @Pointcut("@annotation(annotation)")
    public void callAt(SuccessGauge annotation) {
    }

    @Around("callAt(annotation)")
    public Object count(ProceedingJoinPoint point, SuccessGauge annotation) throws Throwable {
        getCounter(annotation).ifPresent(AtomicInteger::getAndIncrement);
        return point.proceed();
    }

    @AfterThrowing(value = "callAt(annotation)", throwing = "e")
    public void error(JoinPoint point, SuccessGauge annotation, Throwable e) {
        getCounter(annotation).ifPresent(AtomicInteger::getAndDecrement);
    }

    private Optional<AtomicInteger> getCounter(SuccessGauge annotation) {
        if(annotation != null && !StringUtils.isEmpty(annotation.value())) {
            return Optional.of(metricsService.gaugeSingleton(annotation.value(), new AtomicInteger(0)));
        } else {
            log.error("No counter name supplied!");
            return Optional.empty();
        }
    }
}
