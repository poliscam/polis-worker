package net.bramp.ffmpeg;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.io.CharStreams;
import net.bramp.ffmpeg.exception.http.*;
import net.bramp.ffmpeg.io.ProcessUtils;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Private class to contain common methods for both FFmpeg and FFprobe.
 */
abstract class FFcommon {

    /**
     * Path to the binary (e.g. /usr/bin/ffmpeg)
     */
    final String path;

    /**
     * Function to run FFmpeg. We define it like this so we can swap it out (during testing)
     */
    final ProcessFunction runFunc;

    /**
     * Version string
     */
    String version = null;

    private Process process;
    private boolean interrupted = false;

    public FFcommon(@Nonnull String path) {
        this(path, new RunProcessFunction());
    }

    protected FFcommon(@Nonnull String path, @Nonnull ProcessFunction runFunction) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(path));
        this.runFunc = checkNotNull(runFunction);
        this.path = path;
    }

    protected BufferedReader wrapInReader(Process p) {
        return new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8));
    }

    protected void throwOnError(Process p, String s) throws IOException {
        try {
            // TODO In java 8 use waitFor(long timeout, TimeUnit unit)
            if (ProcessUtils.waitForWithTimeout(p, 1, TimeUnit.SECONDS) != 0) {
                // n.viguro: Parse the error
                if(s != null && !s.isEmpty()) {
                    if(s.contains(" 404 ")) {
                        throw new NotFoundException(s);
                    } else if(s.contains(" 4XX ")) {
                        throw new Generic40XException(s);
                    } else if(s.contains(" 400 ")) {
                        throw new BadRequestException(s);
                    } else if(s.contains(" 401 ")) {
                        throw new UnauthorizedRequestException(s);
                    } else if(s.contains(" 403 ")) {
                        throw new ForbiddenRequestException(s);
                    } else if(s.contains(" 500 ") || s.contains(" 5XX ")) {
                        throw new ServerErrorException(s);
                    } else if(s.contains("refused")) {
                        throw new ConnectionRefusedException(s);
                    } else if(s.contains("timed out")) {
                        throw new ConnectionTimedOutException(s);
                    } else {
                        throw new IOException("Unknown exception from ffmpeg: " + s);
                    }
                } else {
                    throw new IOException(path + " returned non-zero exit status. Check stdout.");
                }
            }
        } catch (TimeoutException e) {
            throw new IOException("Timed out waiting for " + path + " to finish.");
        }
    }

    /**
     * Returns the version string for this binary.
     *
     * @return the version string.
     * @throws IOException If there is an error capturing output from the binary.
     */
    public synchronized @Nonnull
    String version() throws IOException {
        if (this.version == null) {
            Process p = runFunc.run(ImmutableList.of(path, "-version"));
            try {
                BufferedReader r = wrapInReader(p);
                this.version = r.readLine();
                CharStreams.copy(r, CharStreams.nullWriter()); // Throw away rest of the output

                throwOnError(p, null);
            } finally {
                p.destroy();
            }
        }
        return version;
    }

    public String getPath() {
        return path;
    }

    /**
     * Returns the full path to the binary with arguments appended.
     *
     * @param args The arguments to pass to the binary.
     * @return The full path and arguments to execute the binary.
     */
    public List<String> path(List<String> args) {
        return ImmutableList.<String>builder().add(path).addAll(args).build();
    }

    /**
     * Runs the binary (ffmpeg) with the supplied args. Blocking until finished.
     *
     * @param args The arguments to pass to the binary.
     * @throws IOException If there is a problem executing the binary.
     */
    public void run(List<String> args) throws IOException {
        checkNotNull(args);

        if (process != null && process.isAlive()) {
            throw new RuntimeException("Can't start a new process, a previous process is still alive!");
        }
        process = runFunc.run(path(args));
        interrupted = false;

        assert (process != null);

        try {
            // TODO Move the copy onto a thread, so that FFmpegProgressListener can be on this thread.

            // Now block reading ffmpeg's stdout. We are effectively throwing away the output.
            //CharStreams.copy(wrapInReader(process), System.out); // TODO Should I be outputting to stdout?

            StringBuilder b = new StringBuilder();
            CharStreams.copy(wrapInReader(process), b);

            if (!interrupted) {
                throwOnError(process, b.toString());
            }

        } finally {
            process.destroy();
        }
    }

    public void interrupt() {
        if (process != null && process.isAlive()) {
            interrupted = true;
            process.destroyForcibly();
        }
    }
}
