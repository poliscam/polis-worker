package net.bramp.ffmpeg.exception.http;

public class ForbiddenRequestException extends IllegalStateException {
    public ForbiddenRequestException() {
    }

    public ForbiddenRequestException(String message) {
        super(message);
    }

    public ForbiddenRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenRequestException(Throwable cause) {
        super(cause);
    }
}
