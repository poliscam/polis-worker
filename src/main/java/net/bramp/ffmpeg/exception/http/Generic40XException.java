package net.bramp.ffmpeg.exception.http;

public class Generic40XException extends IllegalStateException {
    public Generic40XException() {
    }

    public Generic40XException(String message) {
        super(message);
    }

    public Generic40XException(String message, Throwable cause) {
        super(message, cause);
    }

    public Generic40XException(Throwable cause) {
        super(cause);
    }
}
