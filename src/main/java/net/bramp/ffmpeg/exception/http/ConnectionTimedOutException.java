package net.bramp.ffmpeg.exception.http;

public class ConnectionTimedOutException extends IllegalStateException {
    public ConnectionTimedOutException() {
    }

    public ConnectionTimedOutException(String message) {
        super(message);
    }

    public ConnectionTimedOutException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionTimedOutException(Throwable cause) {
        super(cause);
    }
}
