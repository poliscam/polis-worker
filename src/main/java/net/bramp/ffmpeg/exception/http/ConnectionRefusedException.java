package net.bramp.ffmpeg.exception.http;

public class ConnectionRefusedException extends IllegalStateException {
    public ConnectionRefusedException() {
    }

    public ConnectionRefusedException(String message) {
        super(message);
    }

    public ConnectionRefusedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionRefusedException(Throwable cause) {
        super(cause);
    }
}
